package com.example.homework

import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.homework.SignUpActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_log_in.*
import kotlinx.android.synthetic.main.activity_profile.*

class LogInActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_in)
        auth = FirebaseAuth.getInstance()
        sign()
        logIn()
        val pref = PreferenceManager.getDefaultSharedPreferences(this)
        pref.apply {
            val currentUserEmail = getString("CurrentUserEmail", "")
            val currentUserPassword = getString("currentUsePassword", "")
            emailField.setText(currentUserEmail)
            passwordField.setText(currentUserPassword)

        }
        }


    private fun saveUser(){
        logInButton.setOnClickListener(){
            val pref = PreferenceManager.getDefaultSharedPreferences(this)
            val editor = pref.edit()
            val editor2 = pref.edit()

            editor
                .putString("CurrentUserEmail", emailField.text.toString())
                .apply()
            editor2
                .putString("CurrentUserPassword", emailField.text.toString())
                .apply()
        }
    }


    private fun sign() {
        SignUpButton.setOnClickListener {
            val intent = Intent(this, SignUpActivity::class.java)
            startActivity(intent)
        }
    }

    private fun logIn() {
        logInButton.setOnClickListener {
            checkFields()
        }
    }
    private fun checkFields() {
        if (emailField.text.isEmpty() && passwordField.text.isNotEmpty()) {
            Toast.makeText(this, "Please Enter Your Email", Toast.LENGTH_SHORT).show()
        } else if (emailField.text.isNotEmpty() && passwordField.text.isEmpty()) {
            Toast.makeText(this, "Please Enter Your Password", Toast.LENGTH_SHORT).show()
        } else if (emailField.text.isEmpty() && passwordField.text.isEmpty()) {
            Toast.makeText(this, "Please Fill In All Required Fields", Toast.LENGTH_SHORT).show()
        } else {
            logInCheck()
        }
    }
    private fun logInCheck() {
        auth.signInWithEmailAndPassword(emailField.text.toString(), passwordField.text.toString())
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    Log.d("signIn", "signInWithEmail:success")
                    val intent = Intent(this, ProfileActivity::class.java)
                    intent.putExtra("email",emailField.text.toString())
                    startActivity(intent)
                } else {
                    Log.w("signIn", "signInWithEmail:failure", task.exception)
                    Toast.makeText(
                        baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
    }
}

