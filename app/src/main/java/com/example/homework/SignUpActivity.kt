package com.example.homework
import android.content.Intent
import android.nfc.Tag
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.example.homework.LogInActivity
import com.example.homework.R
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_sign_up.*
class SignUpActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        auth = FirebaseAuth.getInstance()
        back()
        checkFields()
    }
    private fun back() {
        Back.setOnClickListener {
            val intent = Intent(this, LogInActivity::class.java)
            startActivity(intent)
        }
    }
    private fun checkFields() {
        SignUp.setOnClickListener {
            if (email.text.isEmpty() && password.text.isNotEmpty() && password2.text.isNotEmpty()) {
                Toast.makeText(this, "Please Enter Your Email", Toast.LENGTH_SHORT).show()
            } else if (email.text.isNotEmpty() && password.text.isEmpty() && password2.text.isEmpty()) {
                Toast.makeText(this, "Please Enter Your Password", Toast.LENGTH_SHORT).show()
            } else if (email.text.isNotEmpty() && password.text.isNotEmpty() && password2.text.isEmpty()) {
                Toast.makeText(this, "Please Re-Enter Your Password", Toast.LENGTH_SHORT).show()
            } else if (email.text.isEmpty() && password.text.isEmpty() or password2.text.isEmpty()) {
                Toast.makeText(this, "Please Fill In All Required Fields", Toast.LENGTH_SHORT)
                    .show()
            } else if (email.text.isNotEmpty() && password.text.toString() != password2.text.toString()) {
                Toast.makeText(this, "Passwords Don't Match", Toast.LENGTH_SHORT).show()
            } else {
                fire()
            }
        }
    }
    private fun fire() {
        auth.createUserWithEmailAndPassword(email.text.toString(), password.text.toString())
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    Toast.makeText(
                        baseContext, "Authentication Success",
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    Toast.makeText(
                        baseContext, "Authentication failed.",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
    }
}
