package com.example.homework


import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.widget.Toast
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_log_in.*
import kotlinx.android.synthetic.main.activity_profile.*


class ProfileActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        logOut()
        setUp()
        linearLayout.setBackgroundColor(Color.parseColor("#FF323232"))
        setNickname()
    }

    private fun logOut(){
        logOutButton.setOnClickListener {
            val pref = PreferenceManager.getDefaultSharedPreferences(this)
            val editor = pref.edit()
            val editor2 = pref.edit()
            editor
                .putString("currentUserEmail", "")
                .apply()
            editor2
                .putString("currentUserPassword", "")
            Toast.makeText(
                baseContext, "თქვენ გამოსული ხართ ანგარიშიდან",
                Toast.LENGTH_SHORT
            ).show()
            emailField.setText("")
            passwordField.setText("")

        }
    }

    private fun setNickname(){
        val database = FirebaseDatabase.getInstance()
        val myRef = database.getReference("user")
        myRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                val value = dataSnapshot.getValue(UserModel::class.java)
                name.text = ("Name: " + value!!.name)
                lastname.text = ("Lastname: " + value!!.lastname)
                nickname.text = ("Nickname: " + value!!.nickname)
                Log.d("onDataChange", "Value is: $value")
            }
            override fun onCancelled(error: DatabaseError) {
                // Failed to read value
                Log.w("onDataChange", "Failed to read value.", error.toException())
            }
        })
    }
    private fun setUp() {
        emailTExtView.text = ((intent.extras!!.getString("email", "")).substringBefore('@')).replace(".", " ")
    }

}

